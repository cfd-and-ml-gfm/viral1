#!/usr/bin/env python
# coding: utf-8

import math
import pandas as pd
import numpy as np
import shutil  
import os, sys
from sklearn.utils import shuffle
from pathlib import Path
import subprocess as sp
import time
import random

if __name__ == "__main__":
    random.seed(1)
    dirpath = "CASESNPZ"
    ranges = []
    ranges.append(list(range(0, 435)))
    ranges.append(list(range(435, 870)))
    ranges.append(list(range(870, 1305)))
    MAXCNT = 100
    dstpath = "SELECTEDNPZ"
    Path(dstpath).mkdir(parents=True, exist_ok=False)
    
    
    for r in ranges:
        cnt = 0
        random.shuffle(r)
        for caseid in r:
            casefile = "%s/case%d.npz" % (dirpath, caseid)
            if os.path.exists(casefile):
                cnt += 1   
                command = "cp %s %s" % (casefile, dstpath)
                #print(command)
                child = sp.Popen(command.split(), stdout=sp.PIPE)
                streamdata = child.communicate()[0]
            if cnt >= MAXCNT:
                break
                
    sys.exit()