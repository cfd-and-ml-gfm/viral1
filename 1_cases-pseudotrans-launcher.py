#!/usr/bin/env python
# coding: utf-8

import math
import pandas as pd
import numpy as np
import shutil  
import os, sys
from sklearn.utils import shuffle
from pathlib import Path
import subprocess as sp
import time

def loadFlows(filepath):
    df = pd.read_excel(filepath, header=None) 
    inlet_list = np.array(df[[0]].values.tolist()[1:]).reshape(-1)
    inlet_list = np.array(np.divide(inlet_list, 3600), dtype=np.float16)
    #print(inlet_list)
    recirc_list = np.array(df[[1]].values.tolist()[1]).reshape(-1)
    recirc_list = np.array(np.divide(recirc_list, 3600), dtype=np.float16)
    #print(recirc_list)
    
    ag1_list = np.array(df[[2]].values.tolist()[1]).reshape(-1)
    #print(ag1_list)
    ag2_list = np.array(df[[3]].values.tolist()[1]).reshape(-1)
    #print(ag2_list)
    
    return inlet_list, recirc_list, ag1_list, ag2_list

def casesParams(inlet_list, recirc_list, ag1_list, ag2_list):
    params_matrix = []
    n_cases = 0
          
    for i in ag2_list:
        for j in ag1_list:
            for k in recirc_list:
                for l in inlet_list:
                    params_matrix.append([n_cases, i, j, k, l])
                    #print(params_matrix[-1])
                    n_cases +=1
    params = np.array(params_matrix)#, dtype=np.float16)
    #np.random.shuffle(params)    
    print("Number of cases: ", n_cases)
    return params                    

def launch2hCase(caseid, params):
    command = "bash prerun.sh REFCASES/case0 %s/case%d %0.4f %0.4f %0.4f %0.4f %d" % (outputDir, caseid, params[4], params[3], params[2], params[1], caseid)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    val = int(streamdata.decode())
    
    for i in range(3):
        command = "bash r2.sh %d %s/case%d %d %d" % (val, outputDir, caseid, caseid, i)
        print(command)
        child = sp.Popen(command.split(), stdout=sp.PIPE)
        streamdata = child.communicate()[0]
        rc = child.returncode
        val = int(streamdata.decode())
    
    command = "bash postrun.sh %d %s/case%d %d" % (val, outputDir, caseid, caseid)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    #val = int(streamdata.decode())

def relaunch2hCase(caseid, params):
    #command = "bash rerun.sh %s/case%d %d %d" % (outputDir, caseid, caseid, i)
    command = "bash rerun.sh %d %s/case%d 2" % (caseid, outputDir, caseid)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    val = int(streamdata.decode())
    
    command = "bash postrun.sh %d %s/case%d %d" % (val, outputDir, caseid, caseid)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    #val = int(streamdata.decode())
 
if __name__ == "__main__":
    case_src = "case-base"
    outputDir = "CASES_PSEUDOTRANS"
    Path(outputDir).mkdir(parents=True, exist_ok=True)
    
    command = "dos2unix postrun.sh r1.sh r2.sh prerun.sh"
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    
    ###
    #Caudales
    ###
    inlet_list, recirc_list, ag1_list, ag2_list = loadFlows('parameters_pseudotrans.xlsx')   
    #recirc_list = [recirc_list[0]]

    ###
    #Parametros del caso
    ###
    params = casesParams(inlet_list, recirc_list, ag1_list, ag2_list)

    if True:
        with open('cases_list.txt', 'w') as f:
            f.write("ID\tAg2\tAg1\tRec\tIn\n")
            for item in params:
                f.write("%d\t%0.4f\t%0.4f\t%0.4f\t%0.4f\n" % (item[0], item[1], item[2], item[3], item[4]))
    
    command = "squeue | wc -l"   
    child = sp.Popen(command, shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
    streamdata = child.communicate()[0]
    cntSlurmJobs = int(streamdata.decode()) - 1 
    #cntSlurmJobs = 0
    
    for idx,case in enumerate(params):
        if cntSlurmJobs > 95:
            break
        casedir = "%s/case%d" % (outputDir, idx)
        if  os.path.isdir(casedir):
            if not os.path.isdir(casedir + "/7201"):
                relaunch2hCase(idx, case)
                cntSlurmJobs += 2 
            else:
                print("%s Done!" % casedir)
        else:
            print("CASEID %d" % idx)
            launch2hCase(idx, case)
            cntSlurmJobs += 5

            