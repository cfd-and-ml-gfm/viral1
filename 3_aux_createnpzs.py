#!/usr/bin/env python
# coding: utf-8

import numpy as np
import subprocess as sp
import os, sys
from numpy import savez_compressed
from pathlib import Path
import shutil  
import glob
from platform import python_version

def createNpArray(tspath):
    vel_filename = "%s/U" % (tspath)
    #print(vel_filename)
    scene = []
    with open(vel_filename, 'r') as reader:
        headerlines = 21 
        head = [next(reader) for x in range(headerlines)]
        ncells = int(reader.readline().strip())
        reader.readline()
        #print(ncells)
        for lineid in range(ncells):
            line = reader.readline()
            lst = list(line[1:-2].split(" "))
            try:
                lstf = list(map(float, lst))
            except Exception as e:
                print(lst)
                lstf = []
                for elem in lst:
                    aux = elem
                    if elem == 'e-0' or elem == '.' or elem == '':
                        aux = 0.0
                    lstf.append(aux)
            scene.append(lstf)    
    return np.array(scene, dtype = np.float16)       
 
if __name__ == "__main__":
    #dirpath = "CASESNPZ"
    dirpath = "CASES_PSEUDOTRANS_NPZS"
    Path(dirpath).mkdir(parents=True, exist_ok=True)
    command = "cp cases_list.txt %s" % (dirpath)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    
    tsdirs = [6301,6401,6501,6601,6701,6801,6901,7001,7101,7201]
    
    #cases = sorted(glob.glob('REFCASES/case*'), key=os.path.basename)
    cases = sorted(glob.glob('CASES_PSEUDOTRANS/case*'), key=os.path.basename)
    for case in cases:
        npzcase = "%s/%s.npz" % (dirpath, case.split("/")[1])
        if os.path.isfile(npzcase):
            pass
        else:
            print(npzcase)
            ds = [] 
            for ts in tsdirs:   
                tspath = "%s/%d" % (case, ts)        
                ds.append(createNpArray(tspath))
            ds = np.array(ds)    
            ds = np.mean(np.array(ds), axis=0)   
            filepath = "%s/%s" % (dirpath, tspath.split("/")[1])
            print("Saving file: %s, with shape:" % (filepath), ds.shape)
            savez_compressed(filepath, data=ds, header=None)
