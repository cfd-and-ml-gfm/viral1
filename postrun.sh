#!/bin/bash
sbatch --parsable --dependency=afterok:$1 <<EOT
#!/bin/bash

#SBATCH --job-name="pos-$3"
#SBATCH -o $2"/%x-%j.out"
##SBATCH -e $2"/%x-%j.err"
#SBATCH --ntasks 1
#SBATCH --time 00-00:03:00

#reconstructPar -case $2 -time 6301:7101 #-fields U
#reconstructPar -case $2 -time 7201
reconstructPar -case $2 -time 6301:7201
#find $2 -mindepth 1 -maxdepth 1 -not \( -name 0 -or -name 1 -or -name system -or -name constant -or -name parallel -or -name params -or -name processor\* \) -exec rm -rf {} +
rm -rf $2/processor*

EOT
