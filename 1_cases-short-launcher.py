#!/usr/bin/env python
# coding: utf-8

import math
import pandas as pd
import numpy as np
import shutil  
import os, sys
from sklearn.utils import shuffle
from pathlib import Path
import subprocess as sp
import time

def loadFlows(filepath):
    df = pd.read_excel(filepath, header=None) 
    inlet_list = np.array(df[[0]].values.tolist()[1:]).reshape(-1)
    #print(len(inlet_list))
    inlet_list = np.array(np.divide(inlet_list, 3600), dtype=np.float16)
    #print(inlet_list)
    recirc_list = np.array(df[[1]].values.tolist()[1:6]).reshape(-1)
    #print(len(recirc_list))
    recirc_list = np.array(np.divide(recirc_list, 3600), dtype=np.float16)
    #print(recirc_list)
    #inlet_list=[0.53199]
    #recirc_list=[0.435996]
    
    ag1_list = np.array(df[[2]].values.tolist()[1:3]).reshape(-1)
    #print(len(ag1_list))
    #print(ag1_list)
    ag2_list = np.array(df[[3]].values.tolist()[1:3]).reshape(-1)
    #print(len(ag2_list))
    #print(ag2_list)
    #ag1_list = [1.25]
    #ag2_list = [1.725]
    
    return inlet_list, recirc_list, ag1_list, ag2_list

def casesParams(inlet_list, recirc_list, ag1_list, ag2_list):
    params_matrix = []
    n_cases = 0
    ag1_def = 1.25
    ag2_def = 1.725
    
    for k in recirc_list:
        for l in inlet_list:
            params_matrix.append([n_cases, ag2_def, ag1_def, k, l])
            #print(params_matrix[-1])
            n_cases +=1
                    
    for i in ag2_list:
        for j in ag1_list:
            if i == ag2_def and j == ag1_def:
                continue
            for k in recirc_list:
                for l in inlet_list:
                    params_matrix.append([n_cases, i, j, k, l])
                    #print(params_matrix[-1])
                    n_cases +=1
    params = np.array(params_matrix)#, dtype=np.float16)
    #np.random.shuffle(params)    
    print("Number of cases: ", n_cases)
    return params                  
    
def launch1hCase(caseid, params):
    #Path("CASES/case%d" % caseid).mkdir(parents=True, exist_ok=False)

    command = "bash prerun.sh REFCASES/case0 CASES/case%d %0.4f %0.4f %0.4f %0.4f %d" % (caseid, params[4], params[3], params[2], params[1], caseid)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    val = int(streamdata.decode())
    
    for i in range(6):
        command = "bash r1.sh %d CASES/case%d %d %d" % (val, caseid, caseid, i)
        print(command)
        child = sp.Popen(command.split(), stdout=sp.PIPE)
        streamdata = child.communicate()[0]
        rc = child.returncode
        val = int(streamdata.decode())
    
    command = "bash postrun.sh %d CASES/case%d %d" % (val, caseid, caseid)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    #val = int(streamdata.decode())    
    
def launch2hCase(caseid, params):
    #Path("CASES/case%d" % caseid).mkdir(parents=True, exist_ok=False)

    command = "bash prerun.sh REFCASES/case0 CASES/case%d %0.4f %0.4f %0.4f %0.4f %d" % (caseid, params[4], params[3], params[2], params[1], caseid)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    val = int(streamdata.decode())
    
    for i in range(3):
        command = "bash r2.sh %d CASES/case%d %d %d" % (val, caseid, caseid, i)
        print(command)
        child = sp.Popen(command.split(), stdout=sp.PIPE)
        streamdata = child.communicate()[0]
        rc = child.returncode
        val = int(streamdata.decode())
    
    command = "bash postrun.sh %d CASES/case%d %d" % (val, caseid, caseid)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    #val = int(streamdata.decode())
 
if __name__ == "__main__":
    case_src = "case-base"
    Path("CASES").mkdir(parents=True, exist_ok=True)
    
    command = "dos2unix postrun.sh r1.sh r2.sh prerun.sh"
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    
    ###
    #Caudales
    ###
    inlet_list, recirc_list, ag1_list, ag2_list = loadFlows('parametres.xlsx')   
    #recirc_list = [recirc_list[0]]

    ###
    #Parametros del caso
    ###
    params = casesParams(inlet_list, recirc_list, ag1_list, ag2_list)

    if True:
        with open('cases_list.txt', 'w') as f:
            f.write("ID\tAg2\tAg1\tRec\tIn\n")
            for item in params:
                f.write("%d\t%0.4f\t%0.4f\t%0.4f\t%0.4f\n" % (item[0], item[1], item[2], item[3], item[4]))
    
    command = "squeue | wc -l"   
    child = sp.Popen(command, shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
    streamdata = child.communicate()[0]
    cntSlurmJobs = int(streamdata.decode()) - 1 
    #cntSlurmJobs = 0
    
    
    if False:
        for idx,case in enumerate(params):
            if cntSlurmJobs > 92:
                break
            if  os.path.isdir("CASES/case%d" % idx) or os.path.isdir("REFCASES/case%d" % idx) or os.path.isdir("FAILCASES/case%d" % idx):
                continue
            else:
                print("CASEID %d" % idx)
                launch1hCase(idx, case)
                cntSlurmJobs += 8
    else:
        for idx,case in enumerate(params):
            if cntSlurmJobs > 95:
                break
            if  os.path.isdir("CASES/case%d" % idx) or os.path.isdir("REFCASES/case%d" % idx) or os.path.isdir("FAILCASES/case%d" % idx):
                continue
            else:
                print("CASEID %d" % idx)
                launch2hCase(idx, case)
                cntSlurmJobs += 5

            