#!/usr/bin/env python
# coding: utf-8

import math
import pandas as pd
import numpy as np
import shutil  
import os, sys
from sklearn.utils import shuffle
from pathlib import Path
import subprocess as sp
import time
import glob

def loadCases(filepath):
    df = pd.read_excel(filepath, header=None) 
    cases_list = np.array(df[[0]].values.tolist()).reshape(-1)
    #print(cases_list)
    times_list = np.array(df[[1]].values.tolist()).reshape(-1)
    times_list = np.ceil(times_list)
    #print(times_list)
    
    return cases_list, times_list
                     
def launchRerun(caseid, path, time):
    command = "bash rerun.sh %d %s %d" % (caseid, path, time)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    #print(streamdata.decode())
    val = int(streamdata.decode())
       
    command = "bash postrun.sh %d %s %d" % (val, path, caseid)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode
    
if __name__ == "__main__":
    case_src = "case-base"
    Path("CASES").mkdir(parents=True, exist_ok=True)
    
    command = "dos2unix postrun.sh rerun.sh"
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode

    cases_list, times_list = loadCases('rerun.xlsx')   
    
    offset = 0
    for idx,case in enumerate(cases_list[offset:]):  
        caseid = int(case.split('case')[1])
        
        path = "CASES/%s" % (case)
        if not os.path.exists(path):
            path = "FAILCASES/%s" % (case)
            if not os.path.exists(path):
                print(case, "does not exist")
                continue
        
        command = "squeue | wc -l"   
        child = sp.Popen(command, shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
        streamdata = child.communicate()[0]
        cntSlurmJobs = int(streamdata.decode()) - 1 
        if cntSlurmJobs > 99:
            break

        print(path)
        ts = []
        for d in glob.glob(path + "/processor0/*"):
            ts.append(int(d.split("/")[-1]))
        ts = sorted(ts)
        lastts = ts[-1]
        for proc in range(16):
            procpath = path + "/processor%d/%d" % (proc, lastts) 
            print(procpath)
            if os.path.exists(procpath):
                command = "rm -rf %s" % procpath
                print(command)
                child = sp.Popen(command.split(), stdout=sp.PIPE)
                streamdata = child.communicate()[0]
        launchRerun(caseid, path, int(times_list[idx+offset]))
        cntSlurmJobs += 2
        
        #break
        #sys.exit()
            