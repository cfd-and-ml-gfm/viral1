#!/usr/bin/env python
# coding: utf-8

import numpy as np
import subprocess as sp
import os, sys
from numpy import savez_compressed
from pathlib import Path
import shutil  
import glob
from platform import python_version

if __name__ == "__main__": 
    command = "dos2unix postrun2.sh"
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    rc = child.returncode

    cases1 = glob.glob('FAILCASES/case*')
    cases2 = glob.glob('CASES/case*')
    cases = cases1 + cases2
    
    for case in cases: 
        if os.path.exists(case + "/6301"):
            print(case)
            command = "bash postrun2.sh 1 %s" % (case)
            print(command)
            child = sp.Popen(command.split(), stdout=sp.PIPE)
            streamdata = child.communicate()[0]
            rc = child.returncode
            #break

