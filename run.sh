#!/bin/bash
sbatch --parsable --dependency=afterok:$1 <<EOT
#!/bin/bash

#SBATCH --job-name="run-$3"
#SBATCH -o $2"/%x-%j.out"
##SBATCH -e $2"/%x-%j.err"
#SBATCH --ntasks 16
#SBATCH --time 00-05:00:00

srun pimpleFoam -case $2 -parallel &> $2/log

EOT