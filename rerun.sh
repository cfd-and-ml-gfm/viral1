#!/bin/bash
#cat > auxfile <<EOT
sbatch --parsable <<EOT
#!/bin/bash

#SBATCH --job-name="rer-$1"
#SBATCH -o $2"/%x-%j.out"
#SBATCH --ntasks 16
#SBATCH --time 00-0$3:00:00

srun pimpleFoam -case $2 -parallel &> $2/log

EOT