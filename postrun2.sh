#!/bin/bash
sbatch --parsable <<EOT
#!/bin/bash

#SBATCH --job-name="pos"
#SBATCH -o $2"/%x-%j.out"
##SBATCH -e $2"/%x-%j.err"
#SBATCH --ntasks 1
#SBATCH --time 00-00:10:00

reconstructPar -case $2 -time 6301:7201
rm -rf $2/processor*

EOT
