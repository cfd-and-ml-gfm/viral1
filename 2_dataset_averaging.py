#!/usr/bin/env python
# coding: utf-8

import numpy as np
import subprocess as sp
import os, sys
from numpy import savez_compressed
from pathlib import Path
import shutil  
import glob
from platform import python_version

def createNpArray(tspath):
    vel_filename = "%s/U" % (tspath)
    #print(vel_filename)
    scene = []
    with open(vel_filename, 'r') as reader:
        headerlines = 21 
        head = [next(reader) for x in range(headerlines)]
        ncells = int(reader.readline().strip())
        reader.readline()
        #print(ncells)
        for lineid in range(ncells):
            line = reader.readline()
            lst = list(line[1:-2].split(" "))
            try:
                lstf = list(map(float, lst))
            except Exception as e:
                print(lst)
                lstf = []
                for elem in lst:
                    aux = elem
                    if elem == 'e-0' or elem == '.' or elem == '':
                        aux = 0.0
                    lstf.append(aux)
            scene.append(lstf)    
    return np.array(scene, dtype = np.float16)       
 
if __name__ == "__main__":
    dirpath = "CASESNPZ"
    #if os.path.exists(dirpath) and os.path.isdir(dirpath):
    #    shutil.rmtree(dirpath)
    Path(dirpath).mkdir(parents=True, exist_ok=True)
    Path("FAILCASES").mkdir(parents=True, exist_ok=True)

    command = "cp cases_list.txt %s" % (dirpath)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    
    """
    failcases = sorted(glob.glob('FAILCASES/case*'), key=os.path.basename)
    for case in failcases:
        #print(case)
        if not os.listdir(case):
            #print("Directory is empty")
            command = "rmdir %s" % (case)
            print(command)
            child = sp.Popen(command.split(), stdout=sp.PIPE)
            streamdata = child.communicate()[0]
            rc = child.returncode
        #else:    
            #print("Directory is not empty")  
    """
    
    tsdirs = [6301,6401,6501,6601,6701,6801,6901,7001,7101,7201]

    cases1 = glob.glob('FAILCASES/case*')
    cases2 = glob.glob('CASES/case*')
    cases = cases1 + cases2
    #cases = sorted(cases1+cases2, key=os.path.basename)    
    #cases = sorted(glob.glob('CASES/case*'), key=os.path.basename)
    #print(cases)
    #sys.exit()
    for case in cases:
        #print(case)
        """
        if not os.listdir(case):
            #print("Directory is empty")
            command = "rmdir %s" % (case)
            print(command)
            child = sp.Popen(command.split(), stdout=sp.PIPE)
            streamdata = child.communicate()[0]
            rc = child.returncode
        else:
        """
        failed = False
        #print("PROCESSING %s" % case)
        for ts in tsdirs:          
            tspath = "%s/%d" % (case, ts)
            #print("\tPROCESSING %s" % tspath)
            if not os.path.isdir(tspath):
                failed = True
                break
        if failed:
            if True:
                #print("%s FAILED (not moving the directory)" % case)
                pass
            else:
                command = "mv %s FAILCASES/%s" % (case, case.split("/")[1])
                print(command)
                child = sp.Popen(command.split(), stdout=sp.PIPE)
                streamdata = child.communicate()[0]
                rc = child.returncode
        else:
            ds = [] 
            for ts in tsdirs:   
                tspath = "%s/%d" % (case, ts)        
                ds.append(createNpArray(tspath))
            ds = np.array(ds)    
            ds = np.mean(np.array(ds), axis=0)   
            filepath = "%s/%s" % (dirpath, tspath.split("/")[1])
            #print("Uncomment this for saving file: %s, with shape:" % (filepath), ds.shape)
            savez_compressed(filepath, data=ds, header=None)
            
            command = "mv %s REFCASES/%s" % (case, tspath.split("/")[1])
            print(command)
            child = sp.Popen(command.split(), stdout=sp.PIPE)
            streamdata = child.communicate()[0]
            rc = child.returncode
