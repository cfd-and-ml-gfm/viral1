#!/usr/bin/env python
# coding: utf-8

import numpy as np
import subprocess as sp
import os, sys
from numpy import savez_compressed
import pathlib  
import shutil  
import glob
from platform import python_version

def get_params_from_file():
    command = "head -n 32 cases_list.txt | tail -n1"
    print(command)
    child = sp.Popen(command, shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
    streamdata = child.communicate()[0]
    params = streamdata.decode().split()

    return float(params[4]), float(params[3])
    
if __name__ == "__main__":
    srcpath = "REFCASES/case0"
    #dstpath = "casePredicted"
    #predictedCase = "paloma-eval/prediction.npz"
    path = "PREDICTIONS_PSEUDOTRANS/"
    
    #casesids = [1114,1203,1206,1478,1654,1656,855,864,781]
    casesids = [0,1,2,4,5,6,7,8,9,10,11]
    for caseid in casesids:
        dstpath = "casePredictedPseudotrans/case%d" % caseid
        #predictedCase = "paloma-eval/prediction%d.npz" % caseid
        predictedCase = "%s/case%d.npz" % (path, caseid)

        if os.path.exists(dstpath) and os.path.isdir(dstpath):
            print("Removing directory %s ..." % (dstpath))
            shutil.rmtree(dstpath)
            
        print("Copying %s in %s ..." % (srcpath, dstpath))
        shutil.copytree(srcpath, dstpath)
        
        command = "rm -rf %s/params %s/6301 %s/6401 %s/6501 %s/6601 %s/6701 %s/6801 %s/6901 %s/7001 %s/7101" % (dstpath, dstpath, dstpath, dstpath, dstpath, dstpath, dstpath, dstpath, dstpath, dstpath)
        print(command)
        child = sp.Popen(command.split(), stdout=sp.PIPE)
        streamdata = child.communicate()[0]
     
        command = "touch %s/foam.foam" % (dstpath)
        print(command)
        child = sp.Popen(command.split(), stdout=sp.PIPE)
        streamdata = child.communicate()[0]
     
        ds = np.load(predictedCase)
        ds = ds.f.data
        #y.append(ds)
        #print(ds.shape)
        
        #print(ds[0])
        #print(ds[-1])
        
        scene = []
        filename = "%s/7201/U" % (dstpath)
        #print(vel_filename)
        header = ""
        footer = ""
        values = "(\n"

        headerlines = 21
       
        p_inlet, p_recirc = get_params_from_file()
            
        with open(filename, 'r') as reader:
            line = reader.readline()
            cntline = 0
            cntarray = 0
            cntfooter = 0
            while line:
                if cntline <= headerlines:
                    header = header + line
                    if cntline == headerlines:
                        ncells = int(line.strip())
                        reader.readline()
                        cntline += 1
                elif cntline > (headerlines + ncells +1):
                    if cntfooter == 12:
                        line = "\tvolumetricFlowRate\tconstant\t%0.4f;\n" % p_inlet;
                    elif cntfooter == 19:
                        line = "\tvolumetricFlowRate\tconstant\t%0.4f;\n" % p_recirc;
                    footer = footer + line
                    cntfooter += 1 
                else:
                    #print(len(ds), cntarray, ds[cntarray])
                    #input()
                    cell = "(%f %f %f)\n" % (ds[cntarray][0], ds[cntarray][1], ds[cntarray][2])
                    values = values + cell
                    cntarray += 1
                line = reader.readline()
                cntline += 1
        
        print("Writing file %s ..." % filename)
        with open(filename, 'w') as writer:     
            writer.write(header)
            writer.write(values)
            writer.write(footer)
