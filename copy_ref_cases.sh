#!/bin/bash
#mkdir ~/scratch/mancomunada-dataset/REFCASES

cnt=1
for case in 0 22 44 66
do
	mkdir ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/0 ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/1 ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/constant ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/system ~/scratch/mancomunada-dataset/CASES/case$case
	cp ~/scratch/PALOMA/sim$cnt/params ~/scratch/mancomunada-dataset/CASES/case$case
	cp ~/scratch/PALOMA/sim$cnt/parallel ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/6301 ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/6401 ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/6501 ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/6601 ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/6701 ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/6801 ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/6901 ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/7001 ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/7101 ~/scratch/mancomunada-dataset/CASES/case$case
	cp -r ~/scratch/PALOMA/sim$cnt/7201 ~/scratch/mancomunada-dataset/CASES/case$case
	rm ~/scratch/mancomunada-dataset/CASES/case$case/U 
	rm ~/scratch/mancomunada-dataset/CASES/case$case/alpha.sludge
	rm ~/scratch/mancomunada-dataset/CASES/case$case/k
	rm ~/scratch/mancomunada-dataset/CASES/case$case/p 
	rm ~/scratch/mancomunada-dataset/CASES/case$case/p_rgh
	rm ~/scratch/mancomunada-dataset/CASES/case$case/nut
	let cnt++
done
